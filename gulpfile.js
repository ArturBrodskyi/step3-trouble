import gulp from "gulp";
const { src, dest, watch, series, parallel } = gulp;

import imagemin from "gulp-imagemin";
import autoprefixer from "gulp-autoprefixer";
import csso from "gulp-csso";
import clean from "gulp-clean";
import * as dartSass from "sass";
import gulpSass from "gulp-sass";
import fileInclude from "gulp-file-include";
import concat from "gulp-concat";
import minify from "gulp-minify";
import bsc from "browser-sync";

const sass = gulpSass(dartSass);
const browserSync = bsc.create();

const htmlTaskHandler = () => {
	return src("./src/**/*.html")
	.pipe(fileInclude({
		prefix: '@@',
		basepath: '@file'
	}))
	.pipe(dest("./dist"))
	.pipe(browserSync.stream());
};

const reset = () => {
	return src("./src/reset/reset.css")
		.pipe(dest("./dist/style"))
		.pipe(browserSync.stream());
}

const cssTaskHandler = () => {
	return src("./src/scss/**/*.scss")
		.pipe(sass().on("error", sass.logError))
		.pipe(autoprefixer())
		.pipe(csso())
		.pipe(dest("./dist/style"))
		.pipe(browserSync.stream());
};

const imagesTaskHandler = () => {
	return src("./src/images/**/*.*")
		.pipe(imagemin())
		.pipe(dest("./dist/images"));
};

const fontTaskHandler = () => {
	return src("./src/fonts/**/*.*")
		.pipe(dest("./dist/fonts"))
		.pipe(browserSync.stream());
};

const additionalFontsTaskHandler = () => {
	return src("./src/fonts/**/*.*")
		.pipe(dest("./dist/fonts"))
		.pipe(browserSync.stream());
};

const cleanDistTaskHandler = () => {
	return src("./dist", { read: false, allowEmpty: true })
		.pipe(clean({ force: true }));
};

const scriptfontTaskHandlerr = () => {
	return gulp.src('./src/scripts/**/*.js')
		.pipe(concat("script.js"))
		.pipe(minify({
			ext: {
				src: ".js",
				min: "min.js"
			}
		}))
		.pipe(gulp.dest('./dist/scripts'))
		.pipe(browserSync.stream());
};

const browserSyncTaskHandler = () => {
	browserSync.init({
		server: {
			baseDir: "./dist",
		}
	});

	watch("./src/scss/**/*.scss").on(
		"all",
		series(cssTaskHandler, browserSync.reload)
	);
	watch("./src/**/*.html").on(
		"change",
		series(htmlTaskHandler, browserSync.reload)
	);
	watch("./src/images/**/*").on(
		"all",
		series(imagesTaskHandler, browserSync.reload)
	);
	watch("./src/scripts/**/*.js").on(
		"all",
		series(scriptfontTaskHandlerr, browserSync.reload)
	);
};

export const cleaning = cleanDistTaskHandler;
export const html = htmlTaskHandler;
export const css = cssTaskHandler;
export const fonts = parallel(fontTaskHandler, additionalFontsTaskHandler);
export const images = imagesTaskHandler;
export const script = scriptfontTaskHandlerr;

export const build = series(
	cleanDistTaskHandler,
	parallel(reset, htmlTaskHandler, cssTaskHandler, fonts, imagesTaskHandler, scriptfontTaskHandlerr)
);

export const dev = series(build, browserSyncTaskHandler);
